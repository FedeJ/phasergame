﻿class Game extends Phaser.Game {
    // -------------------------------------------------------------------------
    constructor() {
        // init game
        super(800, 600, Phaser.CANVAS, "content", State);
    }
}

class State extends Phaser.State {

    private worldHeight = 9999999;

    private platforms: Phaser.Group;
    private player: Phaser.Sprite;
    private cursors: Phaser.CursorKeys;
    private stars: Phaser.Group;

    private scoreText: Phaser.Text;
    private bestScoreText: Phaser.Text;
    private score: number;
    private bestScore: number;
    private startPoint: number;

    private cameraVelocity: number;

    private started: boolean;
    private hitPlatform: boolean;
    private ledgeCounter: number;

    private playerVelocity: number;

    private deltaTime: number;

  

    preload() {
        this.game.load.image('sky', 'assets/sky.png');
        this.game.load.image('ground', 'assets/platform.png');
        this.game.load.image('star', 'assets/star.png');
        this.game.load.spritesheet('dude', 'assets/dude.png', 32, 48);
        this.cursors = this.game.input.keyboard.createCursorKeys();
    }

    create() {
        this.game.physics.startSystem(Phaser.Physics.ARCADE);
        this.game.add.sprite(0, 0, 'sky');

        this.game.world.setBounds(0, 0, 800, this.worldHeight);
        this.game.camera.setPosition(0, this.worldHeight);      

        this.deltaTime = 0;
        this.score = 0;
        this.cameraVelocity = 0;
        this.ledgeCounter = this.game.camera.position.y + 200;

        this.scoreText = this.game.add.text(20, 20, 'Score: 0', { font: 'bold 32px Arial', fill: '#000' });
        this.scoreText.addColor("red", 0);

        this.bestScore = localStorage.getItem("bestScore");
        if (localStorage.getItem("bestScore") == null)
            this.bestScore = 0;
        this.bestScoreText = this.game.add.text(20, 50, 'BestScore: ' + this.bestScore, { font: 'bold 32px Arial', fill: '#000' });
        this.bestScoreText.addColor("red", 0);

        this.platforms = this.game.add.group();
        this.platforms.physicsBodyType = Phaser.Physics.ARCADE;
        this.platforms.enableBody = true;

        /*
        this.stars = this.game.add.group();
        this.stars.enableBody = true;
        */

        this.platforms.classType = Ledge;
        this.platforms.createMultiple(8, 'ground');
        this.platforms.forEach(function (ledge: Ledge) {
            ledge.setUp(0, 0);
        }, this);

        var ledge1 = this.platforms.getFirstDead();
        ledge1.reset(50, this.worldHeight - 250);
        ledge1.scale.setTo(this.game.rnd.realInRange(0.3, 0.5), 1);

        var ledge2 = this.platforms.getFirstDead();
        ledge2.reset(150, this.worldHeight - 450);
        ledge2.scale.setTo(this.game.rnd.realInRange(0.3, 0.5), 1);

        var ground = this.platforms.create(0, this.worldHeight - 64, 'ground');
        ground.scale.setTo(2, 2);
        ground.body.immovable = true;


        /*
        for (var i = 0; i < 12; i++) {
            var star = this.stars.create(i * 70, 0, 'star');
            star.body.gravity.y = 6;
            star.body.bounce.y = 0.7 + Math.random() * 0.2;
        }
        */

        this.player = this.game.add.sprite(32, this.worldHeight - 150, 'dude');
        this.game.physics.arcade.enable(this.player);

        this.player.body.bounce.y = 0.2;
        this.player.body.gravity.y = 5000;
        this.player.body.collideWorldBounds = true;

        this.player.animations.add('left', [0, 1, 2, 3], 10, true);
        this.player.animations.add('right', [5, 6, 7, 8], 10, true);

    }
    update() {
        this.collitions();
        this.pointsController();
        this.deltaTime = this.game.time.elapsed / 15;

        if (!this.started && (this.cursors.down.isDown || this.cursors.up.isDown || this.cursors.right.isDown || this.cursors.left.isDown))
            this.started = true;

        if (!this.started)
            return;

        this.platforms.forEach(function (ledge: Ledge) { ledge.checkInCamera() }, this);
        this.ledgeGenerator();
        this.playerDie();
        this.game.camera.setPosition(this.game.camera.x, this.game.camera.y - 1 - this.cameraVelocity);
        this.playerMovement();
    }

    render() {
        //this.game.debug.spriteInfo(this.player, 32, 32);
        //this.game.debug.text(this.cameraVelocity.toString(), 32, 32);
        this.game.debug.text(this.deltaTime.toString(), 32, 32);
    }

    private ledgeGenerator() {
        if (this.ledgeCounter > this.camera.position.y + 180) {
            this.ledgeCounter = this.camera.position.y;
            var ledge = this.platforms.getFirstDead();
            this.cameraVelocity += 0.05;
            if (this.cameraVelocity > 5)
                this.cameraVelocity = 4.5;
            ledge.alive = true;
            ledge.scale.setTo(this.game.rnd.realInRange(0.3, 0.5), 1);
            ledge.position.setTo(this.game.rnd.integerInRange(15, this.camera.width - 200), this.camera.position.y);
        }
    }

    private pointsController() {
        this.scoreText.position.y = this.game.camera.position.y + 20;
        this.bestScoreText.position.y = this.game.camera.position.y + 50;
        if (this.score < -(this.camera.position.y - this.worldHeight)) {
            this.score = -(this.camera.position.y - this.worldHeight) - this.camera.height;
        this.scoreText.text = 'Score: ' + this.score;
        }
    }

    private playerDie() {
        if (this.player.position.y > this.game.camera.position.y + this.game.camera.height)
        {
            if (this.score > this.bestScore) {
                this.bestScore = this.score;
                localStorage.setItem("bestScore", this.bestScore.toString());
            }
            this.started = false;
            this.hitPlatform = false;
            this.game.state.restart(true, false);
            
        }
    }

    private playerMovement() {

        this.player.body.velocity.x = 0;

        if (this.hitPlatform)
            this.playerVelocity = 600 * this.deltaTime;
        else
            this.playerVelocity = 1200 * this.deltaTime;

        if (this.cursors.left.isDown) {
            this.player.body.velocity.x = -this.playerVelocity;
            this.player.animations.play('left');
        }
        else if (this.cursors.right.isDown) {
            this.player.body.velocity.x = this.playerVelocity;
            this.player.animations.play('right');
        }
        else {
            this.player.animations.stop();
            this.player.frame = 4;
        }
        if (this.cursors.up.isDown && this.player.body.touching.down && this.hitPlatform) {
            this.player.body.velocity.y = -1500;
        }
    }

    /*private collectStar(player: any, star: any) {
        star.kill();
        this.score += 10;
        this.scoreText.text = 'Score: ' + this.score;
    }*/

    private collitions() {
        this.hitPlatform = this.game.physics.arcade.collide(this.player, this.platforms);
        //this.game.physics.arcade.collide(this.stars, this.platforms);
        //this.game.physics.arcade.overlap(this.player, this.stars, this.collectStar, null, this);
    }
}

class Ledge extends Phaser.Sprite {

    public setUp(x: number, y: number) {
        this.reset(x, y);
        this.body.immovable = true;
        this.body.checkCollision.down = false;
        this.body.checkCollision.left = false;
        this.body.checkCollision.right = false;
        this.alive = false;
    }


    public checkInCamera() {
        if (!this.inCamera) {
            this.alive = false;
        }
    }
}

window.onload = () => {
    new Game();
};